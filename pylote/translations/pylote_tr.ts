<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/main.py" line="696"/>
        <source>Quit</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="183"/>
        <source>Open...</source>
        <translation>Aç...</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="223"/>
        <source>Save as...</source>
        <translation>Farklı kaydet...</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="191"/>
        <source>Reload</source>
        <translation>Tekrar Yükle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="199"/>
        <source>Next file</source>
        <translation>Sonraki dosya</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="264"/>
        <source>Configure</source>
        <translation>Yapılandır</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="272"/>
        <source>Minimize</source>
        <translation>Küçült</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="703"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="89"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="391"/>
        <source>Lock instruments</source>
        <translation>Gereçleri kilitle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="430"/>
        <source>Show false cursor</source>
        <translation>Sahte imleci göster</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="289"/>
        <source>New screenshot</source>
        <translation>Yeni ekran görüntüsü</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="303"/>
        <source>White page</source>
        <translation>Beyaz sayfa</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="310"/>
        <source>Dotted paper</source>
        <translation>Noktalı kağıt</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="317"/>
        <source>Grid</source>
        <translation>Izgara</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="324"/>
        <source>Choose background</source>
        <translation>Artalanı seç</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="333"/>
        <source>Ruler</source>
        <translation>Cetvel</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="352"/>
        <source>Square</source>
        <translatorcomment>Geometrik alet kastediliyor</translatorcomment>
        <translation>Gönye</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="371"/>
        <source>Protractor</source>
        <translation>İletki</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="381"/>
        <source>Compass</source>
        <translatorcomment>Geometrik alet kastediliyor</translatorcomment>
        <translation>Pergel</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="704"/>
        <source>Select</source>
        <translation>Seç</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="450"/>
        <source>Line</source>
        <translation>Çizgi</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="457"/>
        <source>Curve</source>
        <translation>Eğri</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="464"/>
        <source>Highlighter</source>
        <translation>Vurgulayıcı</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="471"/>
        <source>Add text</source>
        <translation>Metin ekle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="478"/>
        <source>Add point</source>
        <translation>Nokta ekle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="485"/>
        <source>Add pixmap</source>
        <translation>Piksel haritası ekle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="74"/>
        <source>Color</source>
        <translation>Renk</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="769"/>
        <source>Styles</source>
        <translation>Biçemler</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="68"/>
        <source>Font</source>
        <translation>Yazı Tipi</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="520"/>
        <source>Delete selected object</source>
        <translation>Seçili nesneyi sil</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="527"/>
        <source>Erase all</source>
        <translation>Tümünü sil</translation>
    </message>
    <message>
        <location filename="../libs/utils_instruments.py" line="277"/>
        <source>Point name:</source>
        <translation>Nokta adı:</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="410"/>
        <source>Configuration</source>
        <translation>Yapılandırma</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="447"/>
        <source>Other</source>
        <translation>Diğer</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="115"/>
        <source>&lt;P&gt;&lt;/P&gt;&lt;P ALIGN=LEFT&gt;Here you can select on which screen you will to work.&lt;/P&gt;</source>
        <translation>&lt;P&gt;&lt;/P&gt;&lt;P ALIGN=LEFT&gt;Buradan hangi ekranda çalışmak istediğinizi seçebilirsiniz.&lt;/P&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="62"/>
        <source>Screen usage mode</source>
        <translation>Ekran kullanımı modu</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="66"/>
        <source>All space</source>
        <translation>Tüm alan</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="68"/>
        <source>Full screen</source>
        <translation>Tam ekran</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="96"/>
        <source>Screen number</source>
        <translation>Ekran numarası</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="423"/>
        <source>Screen</source>
        <translation>Ekran</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="215"/>
        <source>Icon size</source>
        <translation>Simge boyutu</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="315"/>
        <source>Enter a value between {0} and {1}:</source>
        <translation>{0} ve {1} arasında bir değer girin:</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="106"/>
        <source>Insert text</source>
        <translatorcomment>&quot;Insert&quot; -&gt; &quot;Araya sokmak&quot; anlamında kullanılıyor. Burada yaygın çeviri olan &quot;eklemek&quot; şeklinde çevirdim.</translatorcomment>
        <translation>Metin ekle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="615"/>
        <source>Tools</source>
        <translation>Araçlar</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="236"/>
        <source>Visible actions</source>
        <translation>Görünür eylemler</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="283"/>
        <source>Screenshot delay</source>
        <translation>Ekran görüntüsü gecikmesi</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="285"/>
        <source>in milliseconds</source>
        <translation>milisaniye olarak</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="431"/>
        <source>Tools window</source>
        <translation>Araçlar penceresi</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="439"/>
        <source>Kids</source>
        <translation>Çocuklar</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="215"/>
        <source>Save</source>
        <translation>Kaydet</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="207"/>
        <source>Previous file</source>
        <translation>Önceki dosya</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="250"/>
        <source>Export SVG</source>
        <translation>SVG olarak dışa aktar</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="506"/>
        <source>Undo</source>
        <translation>Geri al</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="513"/>
        <source>Redo</source>
        <translatorcomment>Son yapılan geri alma eylemini iptal etmek için &quot;tekrarla&quot; kullandım. En son yapılan eylemi üst üste tekrar yapma işine de &quot;yinelemek&quot; diyoruz.</translatorcomment>
        <translation>Tekrarla</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="279"/>
        <source>Create a launcher</source>
        <translation>Bir başlatıcı oluştur</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="401"/>
        <source>Lock units</source>
        <translation>Birimleri kilitle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="409"/>
        <source>Save units</source>
        <translation>Birimleri kaydet</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="423"/>
        <source>Reset units</source>
        <translation>Birimleri sıfırla</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="361"/>
        <source>Square (not graduated)</source>
        <translatorcomment>&quot;square&quot; alet olarak &quot;L cetveli&quot; veya &quot;gönye&quot; oluyor. &quot;not graduated&quot; ise bu aletin üzerinde kademelerin işaretli olmadığının kastedildiğini belirtiyor.</translatorcomment>
        <translation>Gönye (kademesiz)</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="333"/>
        <source>Print configuration (and PDF export)</source>
        <translation>Yazdırma yapılandırması (ve PDF dışa aktarma)</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="339"/>
        <source>Print mode</source>
        <translation>Yazdırma modu</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="341"/>
        <source>Full page</source>
        <translation>Tüm sayfa</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="343"/>
        <source>True size</source>
        <translation>Gerçek boyut</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="354"/>
        <source>Orientation</source>
        <translatorcomment>Sayfanın yönü</translatorcomment>
        <translation>Yön</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="356"/>
        <source>Portrait</source>
        <translation>Dikey</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="358"/>
        <source>Landscape</source>
        <translation>Yatay</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="82"/>
        <source>&lt;p align=left&gt;&lt;b&gt;All space: &lt;/b&gt;the application use all the free space on desktop.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;Full screen: &lt;/b&gt;choose this if you ave problems with All space mode.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=center&gt;&lt;b&gt;If you change this, you need to restart application.&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p align=left&gt;&lt;b&gt;Tüm alan: &lt;/b&gt;uygulama masaüstündeki tüm boş alanı kullanır.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;Tam ekran: &lt;/b&gt;eğer Tüm alan modunda sorun yaşıyorsanız bunu seçin.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=center&gt;&lt;b&gt;Bunu değiştirirseniz uygulamayı yeniden başlatmanız gerekir.&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="378"/>
        <source>&lt;p align=left&gt;&lt;b&gt;Full page: &lt;/b&gt;printing will be adapted to the dimensions of the page.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;True size: &lt;/b&gt;the printed document comply with the dimensions of your figures.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</source>
        <translation>&lt;p align=left&gt;&lt;b&gt;Tam sayfa: &lt;/b&gt;yazdırma sayfanın boyutlarına göre ayarlanacaktır.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;Gerçek boyut: &lt;/b&gt;yazdırılan belge şekillerinizin boyutlarıyla uyumlu olacaktır.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="896"/>
        <source>Custom width</source>
        <translation>Özel genişlik</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="308"/>
        <source>Attach distance</source>
        <translatorcomment>Burada &quot;attach&quot; ile taşıma yaparken nesnelerin arasında ne kadar mesafe kaldığında birbirlerine yapıştırılacakları kastediliyor.</translatorcomment>
        <translation>Yapışma mesafesi</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="310"/>
        <source>between lines or points and ruler or square</source>
        <translation>çizgiler veya noktalar ve cetvel veya gönye arasında</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="416"/>
        <source>Restore backed up units</source>
        <translation>Yedeklenmiş birimleri geri yükle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="499"/>
        <source>Paste</source>
        <translation>Yapıştır</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="667"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation>Masaüstü dosyasının oluşturulacağı klasörü seçin</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="110"/>
        <source>About {0}</source>
        <translation>{0} hakkında</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="70"/>
        <source>(version {0})</source>
        <translation>(sürüm {0})</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="565"/>
        <source>Solid</source>
        <translatorcomment>&quot;Katı&quot; olarak da çevrilebilir ancak burada kenarlık biçimlendirmesinden bahsedildiği için &quot;düz&quot; daha uygun</translatorcomment>
        <translation>Düz</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="572"/>
        <source>Dash</source>
        <translation>Çizgili</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="786"/>
        <source>Select color</source>
        <translation>Renk seçin</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="842"/>
        <source>Select pen width:</source>
        <translation>Kalem kalınlığını seçin:</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="579"/>
        <source>Dot</source>
        <translation>Noktalı</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="586"/>
        <source>Dash Dot</source>
        <translation>Çizgi nokta</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="593"/>
        <source>Dash Dot Dot</source>
        <translation>Çizgi nokta nokta</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="92"/>
        <source>License</source>
        <translation>Lisans</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="97"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="728"/>
        <source>Restore</source>
        <translation>Geri yükle</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="819"/>
        <source>Delete this item ?</source>
        <translation>Bu öge silinsin mi?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="988"/>
        <source>Save File</source>
        <translation>Dosyayı Kaydet</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1252"/>
        <source>Open File</source>
        <translation>Dosya Aç</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1300"/>
        <source>not a valid file</source>
        <translation>geçerli bir dosya değil</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1322"/>
        <source>Failed to open
  {0}</source>
        <translation>Açma başarısız
  {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="526"/>
        <source>Open Image</source>
        <translation>Resim Aç</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="71"/>
        <source>Pylote Files (*.plt)</source>
        <translation>Pylote Dosyaları (*.plt)</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="523"/>
        <source>Image Files</source>
        <translation>Resim Dosyaları</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1061"/>
        <source>Failed to save
  {0}</source>
        <translation>Kaydetme başarısız
  {0}</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1543"/>
        <source>Export as SVG File</source>
        <translation>SVG Dosyası Olarak Dışa Aktar</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1545"/>
        <source>SVG Files (*.svg)</source>
        <translation>SVG Dosyaları (*.svg)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1600"/>
        <source>Export as PDF File</source>
        <translation>PDF Dosyası Olarak Dışa Aktar</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1602"/>
        <source>PDF Files (*.pdf)</source>
        <translation>PDF Dosyaları (*.pdf)</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="697"/>
        <source>Geometry instruments</source>
        <translation>Geometri gereçleri</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="759"/>
        <source>Basic tools</source>
        <translation>Temel araçlar</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="761"/>
        <source>Screenshots and backgrounds</source>
        <translation>Ekran görüntüleri ve artalanlar</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="763"/>
        <source>Drawing tools</source>
        <translation>Çizim araçları</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="870"/>
        <source>click to edit</source>
        <translation>düzenlemek için tıklayın</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="765"/>
        <source>Colors</source>
        <translation>Renkler</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="872"/>
        <source>Width:</source>
        <translation>Genişlik:</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="767"/>
        <source>Widths</source>
        <translation>Genişlikler</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="891"/>
        <source>Custom color</source>
        <translation>Özel renk</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="170"/>
        <source>File</source>
        <translation>Dosya</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="492"/>
        <source>Copy</source>
        <translation>Kopyala</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1573"/>
        <source>Export as PNG File</source>
        <translation>PNG Dosyası Olarak Dışa Aktar</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1575"/>
        <source>PNG Files (*.png)</source>
        <translation>PNG Dosyaları (*.png)</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="257"/>
        <source>Export PNG</source>
        <translation>PNG Dışa Aktar</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="342"/>
        <source>Ruler (not graduated)</source>
        <translation>Cetvel (kademesiz)</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="688"/>
        <source>Recent Files</source>
        <translation>Son Dosyalar</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1097"/>
        <source>No name</source>
        <translation>İsimsiz</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1110"/>
        <source>File must be saved ?</source>
        <translation>Dosya kaydedilmeli?</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="175"/>
        <source>New</source>
        <translation>Yeni</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="296"/>
        <source>Transparent area</source>
        <translation>Şeffaf alan</translation>
    </message>
</context>
</TS>
