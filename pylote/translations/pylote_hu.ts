<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="hu" sourcelanguage="en">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/main.py" line="696"/>
        <source>Quit</source>
        <translation>Kilépés</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="183"/>
        <source>Open...</source>
        <translation>Megnyitás...</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="223"/>
        <source>Save as...</source>
        <translation>Mentés másként...</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="191"/>
        <source>Reload</source>
        <translation>Újratöltés</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="199"/>
        <source>Next file</source>
        <translation>Következő fájl</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="264"/>
        <source>Configure</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="272"/>
        <source>Minimize</source>
        <translation>Elrejtés</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="703"/>
        <source>Help</source>
        <translation>Súgó</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="89"/>
        <source>About</source>
        <translation>Névjegy</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="391"/>
        <source>Lock instruments</source>
        <translation>Segédeszközök zárolása</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="430"/>
        <source>Show false cursor</source>
        <translation>Álkurzor megjelenítése</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="289"/>
        <source>New screenshot</source>
        <translation>Új képernyőkép</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="303"/>
        <source>White page</source>
        <translation>Üres lap</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="310"/>
        <source>Dotted paper</source>
        <translation>Pontozott lap</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="317"/>
        <source>Grid</source>
        <translation>Négyzetrácsos lap</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="324"/>
        <source>Choose background</source>
        <translation>Háttérkép választása</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="333"/>
        <source>Ruler</source>
        <translation>Vonalzó</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="352"/>
        <source>Square</source>
        <translation>Derékszögvonalzó</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="371"/>
        <source>Protractor</source>
        <translation>Szögmérő</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="381"/>
        <source>Compass</source>
        <translation>Körző</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="704"/>
        <source>Select</source>
        <translation>Kijelölés</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="450"/>
        <source>Line</source>
        <translation>Egyenes rajzolása</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="457"/>
        <source>Curve</source>
        <translation>Görbe vonal rajzolása</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="464"/>
        <source>Highlighter</source>
        <translation>Szövegkiemelő</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="471"/>
        <source>Add text</source>
        <translation>Szöveg</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="478"/>
        <source>Add point</source>
        <translation>Pont megadása</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="485"/>
        <source>Add pixmap</source>
        <translation>Pixelkép beszúrása</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="74"/>
        <source>Color</source>
        <translation>Tintaszín</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="769"/>
        <source>Styles</source>
        <translation>Stílus</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="68"/>
        <source>Font</source>
        <translation>Betűtípus</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="520"/>
        <source>Delete selected object</source>
        <translation>A kijelöltek törlése</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="527"/>
        <source>Erase all</source>
        <translation>Az összes törlése</translation>
    </message>
    <message>
        <location filename="../libs/utils_instruments.py" line="277"/>
        <source>Point name:</source>
        <translation>A pont neve:</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="410"/>
        <source>Configuration</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="447"/>
        <source>Other</source>
        <translation>Egyéb</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="115"/>
        <source>&lt;P&gt;&lt;/P&gt;&lt;P ALIGN=LEFT&gt;Here you can select on which screen you will to work.&lt;/P&gt;</source>
        <translation>&lt;P&gt;&lt;/P&gt;&lt;P ALIGN=LEFT&gt;Kiválaszthatod, hogy melyik kijelzőn fogsz dolgozni.&lt;/P&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="62"/>
        <source>Screen usage mode</source>
        <translation>Képernyőmód</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="66"/>
        <source>All space</source>
        <translation>Teljes helykitöltés</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="68"/>
        <source>Full screen</source>
        <translation>Teljes képernyő</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="96"/>
        <source>Screen number</source>
        <translation>A kijelző száma</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="423"/>
        <source>Screen</source>
        <translation>Kijelző</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="215"/>
        <source>Icon size</source>
        <translation>Ikonméret</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="315"/>
        <source>Enter a value between {0} and {1}:</source>
        <translation>Add meg az értéket {0} és {1} között:</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="106"/>
        <source>Insert text</source>
        <translation>Szöveg beszúrása</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="615"/>
        <source>Tools</source>
        <translation>Eszközök</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="236"/>
        <source>Visible actions</source>
        <translation>Megjelenítendő műveletek</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="283"/>
        <source>Screenshot delay</source>
        <translation>A képernyőkép késleltetése</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="285"/>
        <source>in milliseconds</source>
        <translation>milliszekundumokban</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="431"/>
        <source>Tools window</source>
        <translation>Eszközök ablak</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="439"/>
        <source>Kids</source>
        <translation>Gyermekablak</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="215"/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="207"/>
        <source>Previous file</source>
        <translation>Előző fájl</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="243"/>
        <source>Print</source>
        <translation type="obsolete">Nyomtatás</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="251"/>
        <source>Export PDF</source>
        <translation type="obsolete">Exportálás PDF-be</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="250"/>
        <source>Export SVG</source>
        <translation>Exportálás SVG-be</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="506"/>
        <source>Undo</source>
        <translation>Visszavonás</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="513"/>
        <source>Redo</source>
        <translation>Újra végrehajtás</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="279"/>
        <source>Create a launcher</source>
        <translation>Aasztalbeállító fájl készítése</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="401"/>
        <source>Lock units</source>
        <translation>Mérték zárolása</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="409"/>
        <source>Save units</source>
        <translation>Mérték mentése</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="416"/>
        <source>Restore backed up units</source>
        <translation>Mérték visszaállítása</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="423"/>
        <source>Reset units</source>
        <translation>Mérték kezdőértékre állítása</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="361"/>
        <source>Square (not graduated)</source>
        <translation>Derékszögvonalzó (beosztás nélkül)</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="333"/>
        <source>Print configuration (and PDF export)</source>
        <translation>Oldalbeállítás</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="339"/>
        <source>Print mode</source>
        <translation>Nyomtatási mód</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="341"/>
        <source>Full page</source>
        <translation>Teljes lap</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="343"/>
        <source>True size</source>
        <translation>Eredeti méret</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="354"/>
        <source>Orientation</source>
        <translation>Tájolás</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="356"/>
        <source>Portrait</source>
        <translation>Álló</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="358"/>
        <source>Landscape</source>
        <translation>Fekvő</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="378"/>
        <source>&lt;p align=left&gt;&lt;b&gt;Full page: &lt;/b&gt;printing will be adapted to the dimensions of the page.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;True size: &lt;/b&gt;the printed document comply with the dimensions of your figures.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</source>
        <translation>&lt;p align=left&gt;&lt;b&gt;Teljes lap: &lt;/b&gt;A nyomtatás a lapmérethez igazodik.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;Eredeti méret: &lt;/b&gt;a nyomtatott dokumentum mérete a rajzod méretéhez igazodik.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="891"/>
        <source>Custom color</source>
        <translation>Egyéni szín</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="896"/>
        <source>Custom width</source>
        <translation>Egyéni méret</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="308"/>
        <source>Attach distance</source>
        <translation>A tapadás távolsága</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="310"/>
        <source>between lines or points and ruler or square</source>
        <translation>a vonalak vagy pontok és a vonalzók között</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="82"/>
        <source>&lt;p align=left&gt;&lt;b&gt;All space: &lt;/b&gt;the application use all the free space on desktop.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;Full screen: &lt;/b&gt;choose this if you ave problems with All space mode.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=center&gt;&lt;b&gt;If you change this, you need to restart application.&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p align=left&gt;&lt;b&gt;Teljes kitöltés: &lt;/b&gt;az alkalmazás kitölti a teljes munkaasztalt.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=left&gt;&lt;b&gt;Teljes képernyő: &lt;/b&gt;válaszd ezt, ha az előbbi mód problémát okoz.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p align=center&gt;&lt;b&gt;Ennek a beállításnak a megváltoztatása után újra kell indítanod az alkalmazást.&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="499"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="667"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation>Válassz egy könyvtárat az asztalbeállító fájl mentéséhez</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="110"/>
        <source>About {0}</source>
        <translation>A {0} névjegye</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="70"/>
        <source>(version {0})</source>
        <translation>(változat {0})</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="565"/>
        <source>Solid</source>
        <translation>Folyamatos</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="572"/>
        <source>Dash</source>
        <translation>Szaggatott</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="786"/>
        <source>Select color</source>
        <translation>Válasszon színt</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="842"/>
        <source>Select pen width:</source>
        <translation>Tollvastagság:</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="579"/>
        <source>Dot</source>
        <translation>Pontozott</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="586"/>
        <source>Dash Dot</source>
        <translation>Vonal, pont</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="593"/>
        <source>Dash Dot Dot</source>
        <translation>Vonal, pont, pont</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="92"/>
        <source>License</source>
        <translation>Licenc</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="97"/>
        <source>Close</source>
        <translation>Bezár</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="728"/>
        <source>Restore</source>
        <translation>Visszaállítás</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="819"/>
        <source>Delete this item ?</source>
        <translation>Törlöd ezt az elemet?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="988"/>
        <source>Save File</source>
        <translation>Fájl mentése</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1252"/>
        <source>Open File</source>
        <translation>Fájl megnyitása</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1300"/>
        <source>not a valid file</source>
        <translation>nem megfelelő fájl</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1322"/>
        <source>Failed to open
  {0}</source>
        <translation>Nem sikerült megnyitni:
  {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="526"/>
        <source>Open Image</source>
        <translation>Kép megnyitása</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="71"/>
        <source>Pylote Files (*.plt)</source>
        <translation>Pylote fájlok (*.plt)</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="523"/>
        <source>Image Files</source>
        <translation>Képfájlok</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1061"/>
        <source>Failed to save
  {0}</source>
        <translation>Nem sikerült menteni:
  {0}</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1543"/>
        <source>Export as SVG File</source>
        <translation>Exportálás SVG formátumba</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1545"/>
        <source>SVG Files (*.svg)</source>
        <translation>SVG fájlok (*.svg)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1600"/>
        <source>Export as PDF File</source>
        <translation>Exportálás PDF formátumba</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1602"/>
        <source>PDF Files (*.pdf)</source>
        <translation>PDF fájlok (*.pdf)</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="697"/>
        <source>Geometry instruments</source>
        <translation>Geometriai eszközök</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="759"/>
        <source>Basic tools</source>
        <translation>Alapvető eszközök</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="761"/>
        <source>Screenshots and backgrounds</source>
        <translation>Pillanatképek és hátterek</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="763"/>
        <source>Drawing tools</source>
        <translation>Rajzeszközök</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="870"/>
        <source>click to edit</source>
        <translation>kattintson a szerkesztésre</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="765"/>
        <source>Colors</source>
        <translation>Színek</translation>
    </message>
    <message>
        <location filename="../libs/utils_graphicsview.py" line="872"/>
        <source>Width:</source>
        <translation>Szélesség:</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="767"/>
        <source>Widths</source>
        <translation>Szélességben</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="170"/>
        <source>File</source>
        <translation>Fájl</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="492"/>
        <source>Copy</source>
        <translation>Másolat</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1573"/>
        <source>Export as PNG File</source>
        <translation>Exportálás PNG formátumba</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1575"/>
        <source>PNG Files (*.png)</source>
        <translation>PNG fájlok (*.png)</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="257"/>
        <source>Export PNG</source>
        <translation>Exportálás PNG-be</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="342"/>
        <source>Ruler (not graduated)</source>
        <translation>Vonalzó (beosztás nélkül)</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="688"/>
        <source>Recent Files</source>
        <translation>Legutóbbi fájlok</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1097"/>
        <source>No name</source>
        <translation>Névtelen</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1110"/>
        <source>File must be saved ?</source>
        <translation>A fájlt el kell menteni?</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="175"/>
        <source>New</source>
        <translation>Új</translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="296"/>
        <source>Transparent area</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
