# PYLOTE

* **İnternet sitesi:** http://pascal.peter.free.fr/pylote.html
* **E-posta:** pascal.peter at free.fr
* **Lisans:** GNU Genel Kamu Lisansı (sürüm 3)
* **Telif hakkı:** (c) 2008-2022

----

Pylote bilgisayar ekranında çizim yapmaya ve çeşitli geometri gereçlerini kullanmaya yarayan bir yazılımdır.  
Bu bir çoklu platform yazılımıdır, özgürdür (GNU GPL), Python programlama dilinde yazılmıştır ve arayüz için PyQt kullanılmıştır.  
Pylote bir bilgisayar ekranı resmiyle çalışır.  
Bu resimde çizim yapabilir ve geometri gereçlerini kullanabilirsiniz.

----

## GEREKSİNİMLER
* [Python](https://www.python.org): programlama dili
* [Qt](https://www.qt.io): kapsamlı "araç takımı" (görsel kullanıcı arayüzü ve bir çok şey)
* [PyQt](https://riverbankcomputing.com): Python ile Qt arasında bağlantı

#### Kullanılan diğer kütüphaneler ve diğer şeyler
* [marked](https://github.com/chjj/marked): Markdown dosyalarını görüntülemek için
* [Breeze Simgeleri](https://api.kde.org/frameworks/breeze-icons/html/index.html)

----

## PYLOTE'a Katkı Sağlayanlar

#### Programlama:
* Pascal Peter (2008-2022)

#### Yerelleştirme:
* Fransızca : Pascal Peter
* Macarca : Róbert Somogyvári (cr04ch at gmail.com)
* Türkçe : Gökhan Gurbetoğlu ([Pardus GNU/Linux](https://www.pardus.org.tr) & [@ggurbet](https://twitter.com/ggurbet))

#### Diğer:
* Sylvain Bignon ([http://cdpmaths.free.fr](http://cdpmaths.free.fr))  
Ubuntu-fr üzerinde bu programı iyileştirmeme yardım etti,  
Pylote tarafından kullanılan birçok resmi yaptı  
ve çoğu duvar kağıdını yaptı.
