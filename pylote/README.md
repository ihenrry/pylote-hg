# PYLOTE

* **Website:** http://pascal.peter.free.fr/pylote.html
* **Email:** pascal.peter at free.fr
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 2008-2022

----

Pylote is a software to draw on the computer screen and manipulate various instruments of geometry.  
This is a multiplatform software, free (GNU GPL), made in Python (programming language) and PyQt for the GUI.  
Pylote works with a computer screen image.  
In this picture you can draw and manipulate geometry instruments.

----

## REQUIREMENTS
* [Python](https://www.python.org): programming language
* [Qt](https://www.qt.io): "toolkit" comprehensive (graphical user interface and a lot of things)
* [PyQt](https://riverbankcomputing.com): link between Python and Qt

#### Other libraries used and other stuff
* [marked](https://github.com/chjj/marked): to view the Markdown files
* [Breeze Icons](https://api.kde.org/frameworks/breeze-icons/html/index.html)

----

## Authors of PYLOTE

#### Programming:
* Pascal Peter (2008-2022)

#### Localization:
* French : Pascal Peter
* Hungarian : Róbert Somogyvári (cr04ch at gmail.com)
* Turkish : Gökhan Gurbetoğlu ([Pardus GNU/Linux](https://www.pardus.org.tr) & [@ggurbet](https://twitter.com/ggurbet))

#### Other:
* Sylvain Bignon ([http://cdpmaths.free.fr](http://cdpmaths.free.fr))  
who helped me to improve this program via the forum Ubuntu-fr,  
made several images used by Pylote  
and is the author of much of wallpapers.
