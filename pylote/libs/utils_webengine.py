# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------
# This is a part of Pylote project.
# Author:       Pascal Peter
# Copyright:    (C) 2008-2022 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------

"""
DESCRIPTION :
    permet de gérer les différences entre QtWebKit et QtWebEngine.
    La classe MyWebEngineView remplace QWebView et QWebEngineView.
"""

# importation des modules utiles :
import utils, utils_functions

WEB_ENGINE = ''
def changeWebEngine(newValue):
    global WEB_ENGINE
    if WEB_ENGINE == '':
        WEB_ENGINE = newValue
        if utils.TESTS:
            print('*******************************************')
            print('utils_webengine.WEB_ENGINE:', WEB_ENGINE)
            print('*******************************************')

# PyQt5 :
from PyQt5 import QtCore, QtWidgets, QtGui
try:
    from PyQt5.QtWebEngineWidgets import QWebEngineView
    from PyQt5.QtWebEngineWidgets import QWebEnginePage
    changeWebEngine('WEBENGINE')
except ImportError:
    from PyQt5.QtWebKitWidgets import QWebView as QWebEngineView
    from PyQt5.QtWebKitWidgets import QWebPage as QWebEnginePage
    changeWebEngine('WEBKIT')



###########################################################"
#   MYWEBENGINEPAGE ET MYWEBENGINEVIEW
###########################################################"

class MyWebEnginePage(QWebEnginePage):
    """
    """
    def __init__(self, parent=None, linksInBrowser=False):
        super(MyWebEnginePage, self).__init__(parent)
        self.linksInBrowser = linksInBrowser

    def acceptNavigationRequest(self, url,  _type, isMainFrame):
        if self.linksInBrowser:
            if _type == QWebEnginePage.NavigationTypeLinkClicked:
                QtGui.QDesktopServices.openUrl(url)
                return False
        return True



class MyWebEngineView(QWebEngineView):
    """
    linksInBrowser : les liens sont ouverts dans le navigateur
    """
    def __init__(self, parent=None, linksInBrowser=False):
        super(MyWebEngineView, self).__init__(parent)

        self.html = ''
        self.TO_HTML = False
        if WEB_ENGINE == 'WEBENGINE':
            self.setPage(MyWebEnginePage(
                self, linksInBrowser=linksInBrowser))
        else:
            if linksInBrowser:
                self.page().setLinkDelegationPolicy(
                    QWebEnginePage.DelegateAllLinks)
                self.linkClicked.connect(self.linkClickedWebKit)

    def linkClickedWebKit(self, url):
        QtGui.QDesktopServices.openUrl(url)

    def toHtmlCallBack(self, data):
        #print('MyWebEngineView.callBack:', data)
        self.html = data
        self.TO_HTML = True

    def toHtml(self):
        self.html = ''
        self.TO_HTML = False
        if WEB_ENGINE == 'WEBENGINE':
            self.page().toHtml(self.toHtmlCallBack)
        else:
            self.html = self.page().mainFrame().toHtml()
            self.TO_HTML = True
        while not(self.TO_HTML):
            QtWidgets.QApplication.processEvents()
        return self.html


